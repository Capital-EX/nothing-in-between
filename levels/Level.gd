extends Node2D
signal future(blocks)
const directions = {
	up = Vector2.UP * 16,
	down = Vector2.DOWN * 16,
	left = Vector2.LEFT * 16,
	right = Vector2.RIGHT * 16
}

const pull_directions = {
	up = Vector2.DOWN * 16,
	down = Vector2.UP * 16,
	left = Vector2.RIGHT * 16,
	right = Vector2.LEFT * 16
}

export (PackedScene) var next_level
export (int) var buttons = 1

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$Player.connect("move", self, "_on_player_move")
	$Player.connect("rotate", self, "_on_player_rotate")

func _on_player_move(direction, pulling):
	var target = null
	var can_move = true
	for block in $Blocks.get_children():
		var rect = Rect2(block.position - Vector2(8, 8), Vector2(16, 16))
		var check = pull_directions[direction] if pulling else directions[direction]
		var behind = directions[direction] if pulling else pull_directions[direction]
		if rect.has_point($Player.position + check):
			var tile_pos = $TileMap.world_to_map(block.position + directions[direction])
			var tile = $TileMap.get_cellv(tile_pos)
			if tile == 9:
				target = block
			else:
				can_move = false
		elif rect.has_point($Player.position + behind) and pulling:
			can_move = false
			
	
	var tile_pos = $TileMap.world_to_map($Player.position + directions[direction])
	var tile = $TileMap.get_cellv(tile_pos)
	if tile == 9 and can_move:
		$Player.position += directions[direction]
		$Step.play()
		$Slide.pitch_scale = rand_range(0.9, 1.1)
		if target:
			$Slide.play()
			$Slide.pitch_scale = rand_range(0.9, 1.1)
			target.position += directions[direction]

func _on_player_rotate():
	var block = find_near_by_block()
	if block:
		block.rotate_block()

func _input(event):
	if event is InputEventKey and not event.is_echo():
		if event.is_action_pressed("future"):
			emit_signal("future", $Blocks.get_children())
	
func find_near_by_block():
	for block in $Blocks.get_children():
		if abs(block.position.x - $Player.position.x) <= 16:
			if abs(block.position.y - $Player.position.y) <= 16:
				return block
	return null
	