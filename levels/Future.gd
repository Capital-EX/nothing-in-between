extends Node2D
signal simulation_finished(status)
const directions = {
	up = Vector2.UP * 16,
	down = Vector2.DOWN * 16,
	left = Vector2.LEFT * 16,
	right = Vector2.RIGHT * 16
}
const INDETERMINABLE = 1000

var solved = false
export (int) var buttons = 1

func _ready():
	pass

func initialize(present_blocks: Array, tilemap: TileMap, buttons: int):
	if $Blocks.get_child_count() != 0:
		for child in $Blocks.get_children():
			$Blocks.remove_child(child)
			
	for block in present_blocks:
		$Blocks.add_child(block.duplicate())
	
	$TileMap.replace_by(tilemap.duplicate())
	
	self.buttons = buttons

func _future_simulate(present_blocks: Array):
	var movable = []
	var on_button = []
	var blocks = $Blocks.get_children()
	for i in range(present_blocks.size()):
		movable.push_back(true)
		on_button.push_back(false)
		blocks[i].position = present_blocks[i].position
		blocks[i].set_direction(present_blocks[i].get_direction())
		
	var iterations = 0
	var buttons_left = buttons
	# While any block can move
	while any_true(movable) and not solved and iterations < INDETERMINABLE:
		for i in range(blocks.size()):
			var dir = blocks[i].get_direction()
			
			if not movable[i]:
				continue
			# Test for collisions
			for j in range(blocks.size()):
				if i == j: 
					continue
				var rect = Rect2(blocks[j].position - Vector2(8, 8), Vector2(16, 16))
				if rect.has_point(blocks[i].position + directions[dir]):
					movable[i] = false
			
			if not movable[i]:
				if on_button[i] and not movable[i]:
					buttons_left -= 1
				continue
				
			var tile_pos = $TileMap.world_to_map(blocks[i].position + directions[dir])
			var tile = $TileMap.get_cellv(tile_pos)
			match tile:
				0: # right
					blocks[i].position += directions[dir]
					blocks[i].set_direction("right")
					on_button[i] = false
					
				1: # up
					blocks[i].position += directions[dir]
					blocks[i].set_direction("up")
					
					on_button[i] = false
					
				2: # left
					blocks[i].position += directions[dir]
					blocks[i].set_direction("left")
					on_button[i] = false
					
				3: # down
					blocks[i].position += directions[dir]
					blocks[i].set_direction("down")
					on_button[i] = false
					
				4: # jump (requires block to move extra spaces)
					pass
					
				6: # button (move, but then stay in place on future steps)
					blocks[i].position += directions[dir]
					on_button[i] = true
					
				5, 7: # Wall and pillar (stop at current position)
					movable[i] = false
					if on_button[i] and not movable[i]:
						buttons_left -= 1
					
				9, 10: # green and empty (just continue moving)
					blocks[i].position += directions[dir]
					on_button[i] = false
					
			
		iterations += 1
		
	
	if buttons_left == 0:
		emit_signal("simulation_finished", "solved")
	elif iterations >= INDETERMINABLE:
		emit_signal("simulation_finished", "indeterminable")
	else:
		emit_signal("simulation_finished", "unsolved")
	


func any_true(array: Array):
	for b in array:
		if b:
			return b

