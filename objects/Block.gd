class_name BlockBaseClass
extends Sprite

export (String, "right", "up", "down", "left") var dir = "right"

# Called when the node enters the scene tree for the first time.
func _ready():
	set_direction(dir)

func get_direction():
	return dir

func set_direction(dir):
	match dir:
		"right": 
			self.dir = "right"
			rotation_degrees = 0
		"down": 
			self.dir = "down"
			rotation_degrees = 90
		"left": 
			self.dir = "left"
			rotation_degrees = 180
		"up": 
			self.dir = "up"
			rotation_degrees = 270

func rotate_block():
	match dir:
		"right": 
			dir = "down"
			rotation_degrees += 90
		"down": 
			dir = "left"
			rotation_degrees += 90
		"left": 
			dir = "up"
			rotation_degrees += 90
		"up": 
			dir = "right"
			rotation_degrees = 0
