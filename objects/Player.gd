extends Sprite
signal move(direction, pulling)
signal rotate
signal future

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func _input(event):
	if event is InputEventKey and not event.is_echo():
		if event.is_action_pressed("up"):
			emit_signal("move", "up", event.shift)
			
		elif event.is_action_pressed("down"):
			emit_signal("move", "down", event.shift)
			
		elif event.is_action_pressed("left"):
			emit_signal("move", "left", event.shift)
			
		elif event.is_action_pressed("right"):
			emit_signal("move", "right", event.shift)
			
		elif event.is_action_pressed("rotate"):
			emit_signal("rotate")
			
			