# Nothing In-Between

Nothing In-Between is a small puzzle game where you can see the present and the future but nothing in-between.  Push blocks and look into the future to solve puzzles.

Developed as part of [Open Jam 2019](https://itch.io/jam/open-jam-2019).

All images are released under [CC by 4.0](https://creativecommons.org/licenses/by/4.0/). Sound effects are released under CC0.

m6x11 font is owned by [Daniel Linssen](https://managore.itch.io/m6x11).  Free to use with attribution.

## How to Edit?

1. Download and install [Godot 3.2](https://godotengine.org/)
2. Import project from projects menu
3. Open project

## Open Source Tools Used

1. [Godot (The game)](https://godotengine.org/)
2. [jsfxr (Step and slide sfw)](http://github.grumdrig.com/jsfxr/)

## Non-Open Source Tools Used

1. [Aseprite (Sprites)](https://www.aseprite.org/)
2. [SunVox (Time warp sound sfx)](https://warmplace.ru/soft/sunvox/)
