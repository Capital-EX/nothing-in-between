extends Node2D
onready var Future = $Viewport2/Future
onready var Present = $Viewport/LevelOne
export (PackedScene) var skip_to
export (bool) var skip

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().paused = true
	if skip_to and skip:
		var next = skip_to.instance()
		Present.queue_free()
		$Viewport.add_child(next)
		Present = next
	Present.connect("future", self, "_future_effect")
	Future.initialize(Present.get_node("Blocks").get_children(), Present.get_node("TileMap"), Present.buttons)
	Future.connect("simulation_finished", self, "_simulation_finished")
# warning-ignore:return_value_discarded
	$CanvasLayer/Control.connect("next_level", self, "_on_next_level")

func _input(event):
	$Viewport.input(event)
	
func _simulation_finished(status):
	match status:
		"solved":
			$AnimationPlayer.play("reveal")
			$AnimationPlayer.connect("animation_finished", self, "_level_solved", [], CONNECT_ONESHOT)
		
		"unsolved":
			$AnimationPlayer.play("reveal")
		
		"indeterminable":
			$AnimationPlayer.play("error")
	
func _level_solved(animation):
	$CanvasLayer/Control.show()
	get_tree().paused = true

func _obscured(__):
	Future._future_simulate(Present.get_node("Blocks").get_children())

func _future_effect(__):
	$CanvasLayer/Panel.hide()
	$AnimationPlayer.play("obscure")
	$AnimationPlayer.connect("animation_finished", self, "_obscured", [], CONNECT_ONESHOT)

func _on_next_level():
	if not Present.next_level:
		return
	var next = Present.next_level.instance()
	Present.queue_free()
	$Viewport.add_child(next)
	Present = next
	Present.connect("future", self, "_future_effect")
	Future.initialize(Present.get_node("Blocks").get_children(), Present.get_node("TileMap"), Present.buttons)
